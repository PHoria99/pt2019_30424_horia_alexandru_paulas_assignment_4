package presentationLayer;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class WaiterGUI {

	private JFrame frmWaiter;
	private Restaurant restaurant;
	private JComboBox comboBox_1, comboBox;
	private JTable table;
	private JScrollPane scrollPane;
	private Order currentOrder;
	private ArrayList<MenuItem> items = new ArrayList<>();
	/**
	 * Create the application.
	 */
	public WaiterGUI(Restaurant restaurant) {
		this.restaurant = restaurant;
		initialize();
		this.frmWaiter.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmWaiter = new JFrame();
		frmWaiter.setTitle("Waiter");
		frmWaiter.setBounds(100, 100, 450, 300);
		this.frmWaiter.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frmWaiter.getContentPane().add(panel, BorderLayout.CENTER);
		
		JLabel lblItem = DefaultComponentFactory.getInstance().createLabel("Item");
		panel.add(lblItem);
		
		comboBox = new JComboBox();
		panel.add(comboBox);
		
		JLabel lblTable = DefaultComponentFactory.getInstance().createLabel("Table");
		panel.add(lblTable);
		
		comboBox_1 = new JComboBox();
		panel.add(comboBox_1);
		fillBoxes();
		JButton btnNewButton = new JButton("Add Item");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				items.add(findMenuItem(comboBox.getSelectedItem().toString()));
			}
		});
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Add Order");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentOrder = new Order();
				currentOrder.Table = (int) comboBox_1.getSelectedItem();
				currentOrder.OrderID = restaurant.orders.size();
				currentOrder.Date = new Date();
				restaurant.orders.add(currentOrder);
				restaurant.customerOrders.put(currentOrder, items);
				items = new ArrayList<>();
				fillTable();
			}
		});
		panel.add(btnNewButton_1);
		
		JButton btnPrintBill = new JButton("Print Bill");
		btnPrintBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Order selectedOrder = findOrder((int) table.getValueAt(table.getSelectedRow(), 0));
					restaurant.createBill(selectedOrder.OrderID);
					restaurant.orders.remove(selectedOrder);
					restaurant.customerOrders.remove(selectedOrder);
					fillTable();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		panel.add(btnPrintBill);

		scrollPane = new JScrollPane();
		table = new JTable();
		fillTable();
		scrollPane.setViewportView(table);
		panel.add(scrollPane);
	}
	
	@SuppressWarnings("unchecked")
	private void fillBoxes()
	{
		for(int i = 1; i <= Restaurant.tables; i++) {
			this.comboBox_1.addItem(i);
		}
		for(MenuItem m : restaurant.menu) {
			this.comboBox.addItem(m.getName());
		}	
	}
	
	private void fillTable()
	{
		this.table.removeAll();
		Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
		Vector<String> name = new Vector<>();
		name.add("OrderID"); name.add("Table"); name.add("Items"); 
		
		for(Order o: restaurant.orders) {
			Vector<Object> row = new Vector<>();
			row.add(o.OrderID);
			row.add(o.Table);
			String products = new String();
			for(MenuItem m:restaurant.customerOrders.get(o)) {
				products+=m.getName() + ", ";
			}
			row.add(products);
			rows.add(row);
		}
		
		table.setModel(new DefaultTableModel(rows,name));
	}
	
	private MenuItem findMenuItem(String name) {
		for(MenuItem m : restaurant.menu) {
			if(m.getName().equals(name)) return m;
		}
		return null;
	}
	
	private Order findOrder(int id) {
		for(Order o: restaurant.orders) {
			if(o.OrderID == id) return o;
		}
		return null;
	}
}
