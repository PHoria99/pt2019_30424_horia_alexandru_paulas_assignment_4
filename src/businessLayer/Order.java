package businessLayer;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


public class Order implements Serializable{
	private static final long serialVersionUID = 1L;
	public int OrderID, Table;
	public Date Date;
	
	public Order() {
		OrderID = 0;
		Table = 1;
		Date = new Date();
	}
	
	public Order(int ID, int table, Date date)
	{
		this.OrderID = ID;
		this.Table = table;
		this.Date = date;
	}
	
	public String toString()
	{
		return new String("Order: OrderID=" + this.OrderID + ", Table=" + this.Table + ", Date=" + this.Date + "!");
	}
	
	public int hashCode() 
	{
		return Objects.hash(this.Date, this.Table, this.OrderID);
	}
	
	public boolean equals(Object obj)
	{
		if(obj == null || this.getClass() != obj.getClass()) return false;
		if(this == obj) return true;
		Order order = (Order)obj;
		if(order.Date.equals(this.Date) && this.OrderID == order.OrderID && this.Table == order.Table) return true;
		else return false;
	}
	
}
