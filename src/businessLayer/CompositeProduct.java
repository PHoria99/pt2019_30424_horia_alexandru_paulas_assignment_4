package businessLayer;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8680868837769347526L;
	private int price;
	public ArrayList<MenuItem> items = new ArrayList<MenuItem>();

	public CompositeProduct() {
		this.setName("Pizza");
	}
	
	public CompositeProduct(String name) {
		this.setName(name);
	}
	
	public void add(MenuItem p){
		this.price = this.price + p.getPrice()*4;
		this.items.add(p);
	}
	
	public String getComponents() {
		StringBuilder components = new StringBuilder();
		for(MenuItem p : this.items) {
			components.append(p.getName() + " ");
		}
		
		return new String(components);
	}	
	
	public int getPrice() {
		return this.price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
}
