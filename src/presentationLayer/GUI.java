package presentationLayer;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import businessLayer.Restaurant;
import dataLayer.FileWriter;

public class GUI {
	private JFrame frame1;
	private JPanel panelSaveLoad;
	private JButton save;
	private Restaurant restaurant;
	
	GUI(Restaurant restaurant)
	{
		this.restaurant = restaurant;
		this.initialize();
	}

	@SuppressWarnings("rawtypes")
	private void initialize()
	{
		//THAT SAVE LOAD BUTTON THINGY
		this.frame1 = new JFrame();
		this.frame1.setSize(100, 100);
		this.frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.panelSaveLoad = new JPanel(new FlowLayout());
		
		this.save = new JButton("Save");
		
		this.save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					FileWriter.serialize(restaurant.menu, restaurant.orders, restaurant.customerOrders);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		this.panelSaveLoad.add(this.save);
		
		this.frame1.add(this.panelSaveLoad);
		this.frame1.setVisible(true);
		
		
		//admin: create, delete, update menu item
		//waiter: choose items for a given table and order
		//chef: just a table with what he gotta cook (THE COMPOSITE STUFF)
		
	}
	
}
