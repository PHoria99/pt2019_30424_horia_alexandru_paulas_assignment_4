package dataLayer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import businessLayer.MenuItem;
import businessLayer.Order;

public class RestaurantSerializator {
	public static ArrayList<MenuItem> loadMenu() throws FileNotFoundException, IOException, NumberFormatException, ClassNotFoundException
	{
		ArrayList<MenuItem> menu = new ArrayList<>();
		ObjectInputStream objectsIn = new ObjectInputStream(new FileInputStream("./temp/menu.ser"));
		
		int size = objectsIn.readInt();
		for(int i = 0; i < size; i++)
		{
			MenuItem item = (MenuItem)objectsIn.readObject();
			menu.add(item);
		}
		objectsIn.close();
		return menu;
	}
	
	public static ArrayList<Order> loadOrders() throws FileNotFoundException, IOException, NumberFormatException, ClassNotFoundException
	{
		ArrayList<Order> orders = new ArrayList<>();
		ObjectInputStream objectsIn = new ObjectInputStream(new FileInputStream("./temp/orders.ser"));
		int size = objectsIn.readInt();
		for(int i = 0; i < size; i++)
		{
			orders.add((Order)objectsIn.readObject());
		}
		objectsIn.close();
		
		return orders;
	}
	
	@SuppressWarnings("unchecked")
	public static HashMap<Order, ArrayList<MenuItem>> loadCustomerOrders() throws FileNotFoundException, IOException, NumberFormatException, ClassNotFoundException
	{
		HashMap<Order, ArrayList<MenuItem> > customerOrders = new HashMap<Order, ArrayList<MenuItem> >();
		ArrayList<Order> orders = loadOrders();
		ObjectInputStream objectsIn = new ObjectInputStream(new FileInputStream("./temp/customer_orders.ser"));
		for(Order o : orders)
		{
			customerOrders.put(o, (ArrayList<MenuItem>)objectsIn.readObject());
		}
		objectsIn.close();
		
		return customerOrders;
	}
}
