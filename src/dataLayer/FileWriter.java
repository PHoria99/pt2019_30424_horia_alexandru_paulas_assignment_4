package dataLayer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import businessLayer.MenuItem;
import businessLayer.Order;

public class FileWriter{
	public static void serialize(ArrayList<MenuItem> menu, ArrayList<Order> orders, HashMap<Order, ArrayList<MenuItem>> customerOrders)
	throws IOException{
			File files = new File("./temp/menu.ser");
			files.createNewFile();

			files = new File("./temp/orders.ser");
			files.createNewFile();

			files = new File("./temp/customer_orders.ser");
			files.createNewFile();
			
			FileOutputStream menuFile = new FileOutputStream("./temp/menu.ser");
			FileOutputStream orderFile = new FileOutputStream("./temp/orders.ser");
			FileOutputStream customerOrdersFile = new FileOutputStream("./temp/customer_orders.ser");
			ObjectOutputStream out1 = new ObjectOutputStream(menuFile);
			ObjectOutputStream out2 = new ObjectOutputStream(orderFile);
			ObjectOutputStream out3 = new ObjectOutputStream(customerOrdersFile);
			out1.writeInt(menu.size()); out2.writeInt(orders.size());
			for(MenuItem m : menu) {
				out1.writeObject(m);
			}
			for(Order o:orders) {
				out2.writeObject(o);
				out3.writeObject(customerOrders.get(o));
			}
			out1.close();out2.close();out3.close();
			menuFile.close();orderFile.close();customerOrdersFile.close();
			System.out.println("Saved entries.");
		
	}
}
